#!/usr/bin/env -S bash -e

# Cleaning the TTY.
clear

echo -e '     ___           ___           ___                                  ___           ___           ___           ___     '
echo -e '    /\__\         /\  \         /\__\                                /\  \         /\  \         /\__\         /\  \    '
echo -e '   /:/ _/_       /::\  \       /:/ _/_         ___                  /::\  \       /::\  \       /:/  /         \:\  \   '
echo -e '  /:/ /\__\     /:/\:\  \     /:/ /\  \       /\__\                /:/\:\  \     /:/\:\__\     /:/  /           \:\  \  '
echo -e ' /:/ /:/  /    /:/ /::\  \   /:/ /::\  \     /:/  /               /:/ /::\  \   /:/ /:/  /    /:/  /  ___   ___ /::\  \ '
echo -e '/:/_/:/  /    /:/_/:/\:\__\ /:/_/:/\:\__\   /:/__/               /:/_/:/\:\__\ /:/_/:/__/___ /:/__/  /\__\ /\  /:/\:\__\'
echo -e '\:\/:/  /     \:\/:/  \/__/ \:\/:/ /:/  /  /::\  \               \:\/:/  \/__/ \:\/:::::/  / \:\  \ /:/  / \:\/:/  \/__/'
echo -e ' \::/__/       \::/__/       \::/ /:/  /  /:/\:\  \               \::/__/       \::/~~/~~~~   \:\  /:/  /   \::/__/     '
echo -e '  \:\  \        \:\  \        \/_/:/  /   \/__\:\  \               \:\  \        \:\~~\        \:\/:/  /     \:\  \     '
echo -e '   \:\__\        \:\__\         /:/  /         \:\__\               \:\__\        \:\__\        \::/  /       \:\__\    '
echo -e '    \/__/         \/__/         \/__/           \/__/                \/__/         \/__/         \/__/         \/__/    '

# Selecting a kernel to install. 
kernel_selector () {
    echo "List of kernels:"
    echo "1) Stable — Vanilla Linux kernel and modules, with a few patches applied."
    echo "2) Hardened — A security-focused Linux kernel."
    echo "3) Longterm — Long-term support (LTS) Linux kernel and modules."
    echo "4) Zen Kernel — Optimized for desktop usage."
    read -r -p "Insert the number of the corresponding kernel: " choice
    echo "$choice will be installed"
    case $choice in
        1 ) kernel=linux
            ;;
        2 ) kernel=linux-hardened
            ;;
        3 ) kernel=linux-lts
            ;;
        4 ) kernel=linux-zen
            ;;
        * ) echo "You did not enter a valid selection."
            kernel_selector
    esac
}

# Selecting a way to handle internet connection. 
network_selector () {
    echo "Network utilities:"
    echo "1) IWD — iNet wireless daemon is a wireless daemon for Linux written by Intel (WiFi-only)."
    echo "2) NetworkManager — Program for providing detection and configuration for systems to automatically connect to networks (both WiFi and Ethernet)."
    echo "3) wpa_supplicant — It's a cross-platform supplicant with support for WEP, WPA and WPA2 (WiFi-only, a DHCP client will be automatically installed too.)"
    echo "4) I will do this on my own."
    read -r -p "Insert the number of the corresponding networking utility: " choice
    echo "$choice will be installed"
    case $choice in
        1 ) echo "Installing IWD."    
            pacstrap /mnt iwd
            echo "Enabling IWD."
            systemctl enable iwd --root=/mnt &>/dev/null
            ;;
        2 ) echo "Installing NetworkManager."
            pacstrap /mnt networkmanager
            echo "Enabling NetworkManager."
            systemctl enable NetworkManager --root=/mnt &>/dev/null
            ;;
        3 ) echo "Installing wpa_supplicant and dhcpcd."
            pacstrap /mnt wpa_supplicant dhcpcd
            echo "Enabling wpa_supplicant and dhcpcd."
            systemctl enable wpa_supplicant --root=/mnt &>/dev/null
            systemctl enable dhcpcd --root=/mnt &>/dev/null
            ;;
        4 )
            ;;
        * ) echo "You did not enter a valid selection."
            network_selector
    esac
}

# Checking the microcode to install.
CPU=$(grep vendor_id /proc/cpuinfo)
if [[ $CPU == *"AuthenticAMD"* ]]
then
    microcode=amd-ucode
else
    microcode=intel-ucode
fi

# Selecting the target for the installation.
PS3="Select the disk where Arch Linux is going to be installed: "
select ENTRY in $(lsblk -dpnoNAME|grep -P "/dev/sd|nvme|vd");
do
    DISK=$ENTRY
    echo "Installing Arch Linux on $DISK."
    break
done

# Deleting old partition scheme.
read -r -p "This will delete the current partition table on $DISK. Do you agree [y/N]? " response
response=${response,,}
if [[ "$response" =~ ^(yes|y)$ ]]
then
    wipefs -af "$DISK" &>/dev/null
    sgdisk -Zo "$DISK" &>/dev/null
else
    echo "Quitting."
    exit
fi

# Creating a new partition scheme.
echo "Creating new partition scheme on $DISK."
parted -s "$DISK" \
    mklabel gpt \
    mkpart ESP 1MiB 513MiB \
    set 1 esp on \
    mkpart archlinux 513MiB 100% \

# Informing the Kernel of the changes.
echo "Informing the Kernel about the disk changes."
partprobe "$DISK"

ESP="/dev/disk/by-partlabel/ESP"
root="/dev/disk/by-partlabel/archlinux"

# Formatting the ESP as FAT32.
echo "Formatting the EFI Partition as FAT32."
mkfs.vfat $ESP &>/dev/null

BTRFS=$root

# Formatting root partition as BTRFS.
echo "Formatting the partition as BTRFS."
mkfs.btrfs -f $BTRFS &>/dev/null

echo "Mounting BTRFS partition to /mnt"
mount $BTRFS /mnt

echo "Mounted successfully"

# Creating BTRFS subvolumes.
echo "Creating BTRFS subvolumes."
btrfs su cr /mnt/@ &>/dev/null
btrfs su cr /mnt/@home &>/dev/null
btrfs su cr /mnt/@snapshots &>/dev/null
btrfs su cr /mnt/@var_log &>/dev/null

# Mounting the newly created subvolumes.
umount /mnt
echo "Mounting the newly created subvolumes."
mount -o ssd,noatime,space_cache,compress=zstd,discard=async,subvol=@ $BTRFS /mnt
mkdir -p /mnt/{home,.snapshots,/var/log,boot}
mount -o ssd,noatime,space_cache=v2,compress=zstd,discard=async,subvol=@home $BTRFS /mnt/home
mount -o ssd,noatime,space_cache=v2,compress=zstd,discard=async,subvol=@snapshots $BTRFS /mnt/.snapshots
mount -o ssd,noatime,space_cache=v2,compress=zstd,discard=async,subvol=@var_log $BTRFS /mnt/var/log
#chattr +C /mnt/var/log idk ???
mount $ESP /mnt/boot/

kernel_selector

# Pacstrap (setting up a base sytem onto the new root).
echo "Installing the base system (it may take a while)."
pacstrap /mnt base $kernel $microcode linux-firmware btrfs-progs efibootmgr snapper reflector base-devel zsh neovim git

network_selector

# Generating /etc/fstab.
echo "Generating a new fstab."
genfstab -U /mnt >> /mnt/etc/fstab

# Setting hostname.
read -r -p "Please enter the hostname: " hostname
echo "$hostname" > /mnt/etc/hostname

# Setting up locales.
read -r -p "Please insert the locale you use (format: xx_XX): " locale
echo "$locale.UTF-8 UTF-8"  > /mnt/etc/locale.gen
echo "LANG=$locale.UTF-8" > /mnt/etc/locale.conf

# Setting up keyboard layout.
read -r -p "Please insert the keyboard layout you use: " kblayout
echo "KEYMAP=$kblayout" > /mnt/etc/vconsole.conf

# Setting hosts file.
echo "Setting hosts file."
cat > /mnt/etc/hosts <<EOF
127.0.0.1   localhost
::1         localhost
127.0.1.1   $hostname.localdomain   $hostname
EOF

# Configuring /mnt/etc/mkinitcpio.conf.
echo "Configuring /mnt/etc/mkinitcpio.conf"
sed -i '7s/MODULES=()/MODULES=(btrfs)/' /mnt/etc/mkinitcpio.conf

cp /root/fast-arch/sysconfig.sh /mnt
cp /root/fast-arch/postinstall.sh /mnt
# Configuring the system.    
arch-chroot /mnt /bin/bash -e /sysconfig.sh
exit

#Copying postinstall to /mnt
echo "Copying fast-arch to your user's home"
cp -r fast-arch /mnt/home/$username/fast-arch

# Finishing up
echo "Done, you may now wish to reboot (further changes can be done by chrooting into /mnt)."
exit
