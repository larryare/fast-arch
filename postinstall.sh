#!/bin/bash
echo "Installing Pipewire"
pacman -S pipewire pipewire-jack pipewire-pulse pipewire-alsa alsa-utils --noconfirm
echo "Installing KDE"
pacman -Sy xorg plasma-meta kde-utilities-meta kde-system-meta --noconfirm
systemctl enable sddm
echo "Installing Nvidia"
pacman -S nvidia --noconfirm
echo "Installing bluetooth"
pacman -S bluez bluez-utils --noconfirm
systemctl enable bluetooth
echo "Installing paru"
sudo pacman -S --needed base-devel rust --noconfirm
sudo -i -u lkere bash << EOF
cd ~/Downloads
git clone https://aur.archlinux.org/paru.git
cd paru
makepkg -si --noconfirm
echo "Installing custom apps"
#sudo -u lkere 
paru -Syyu firefox kitty obsidian brave-bin steam protonup lutris bitwarden vscodium helvum syncthing qbittorrent bitwig-studio --noconfirm
EOF
reboot
