# Setting up timezone.
ln -sf /usr/share/zoneinfo/$(curl -s http://ip-api.com/line?fields=timezone) /etc/localtime &>/dev/null

# Setting up clock.
hwclock --systohc
# Generating locales.
echo "Generating locales."
locale-gen &>/dev/null
# Generating a new initramfs.
echo "Creating a new initramfs."
mkinitcpio -P &>/dev/null
# Snapper configuration
umount /.snapshots
rm -r /.snapshots

#TODO nodbus?
snapper --no-dbus -c root create-config /
btrfs subvolume delete /.snapshots &>/dev/null
mkdir /.snapshots
mount -a
chmod 750 /.snapshots
# Installing GRUB.
echo "Installing systemd-boot on /boot."
bootctl install

# Creating systemd-boot config file.
#TODO systemd-boot-pacman-hook from aur
echo "Creating systemd-boot entry."
    
cat > /boot/loader/loader.conf <<EOF
default  arch.conf
timeout  3
console-mode max
editor   no
EOF

#TODO fix ucode for intel
cat > /boot/loader/entries/arch.conf <<EOF
title   Arch Linux
linux   /vmlinuz-linux
initrd  /amd-ucode.img
initrd  /initramfs-linux.img
options root="PARTLABEL=archlinux" rootflags=subvol=@ rw
EOF

# Setting username.
read -r -p "Please enter name for a user account (enter empty to not create one): " username
# Adding user with sudo privilege
if [ -n "$username" ]; then
    echo "Adding $username with root privilege."
    useradd -mG wheel $username
    echo "$username ALL=(ALL) ALL" >> /etc/sudoers.d/$username
fi

echo "Setting root password"
passwd
echo "Setting user password"
passwd $username

# Enabling Reflector timer.
echo "Enabling Reflector."
systemctl enable reflector.timer &>/dev/null

# Enabling Snapper automatic snapshots.
echo "Enabling Snapper and automatic snapshots entries."
systemctl enable snapper-timeline.timer &>/dev/null
systemctl enable snapper-cleanup.timer &>/dev/null

#Enabling systemd-oomd out of memory manager.
echo "Enabling systemd-oomd."
systemctl enable systemd-oomd &>/dev/null
